<!--=============== footer ===============-->
<div class="height-emulator"></div>
<footer>
	<div class="footer-inner">
		<div class="row">
			<div class="col-md-5">
				<a class="footer-logo ajax" href="index.php"><img style="height: 50;" src="images/footer-logo.png" alt=""></a>
			</div>
			<div class="col-md-3">
				<div class="footer-adress">
					<span>Santa Ana, San José, Costa Rica </span>
					
				</div>
			</div>
			<div class="col-md-4">
				<ul class="footer-contact">
					<li>+ Tel: (506) 2282+3822<br>+ Fax: (506) 2282+8476
					</li>
					<li><a href="mailto:info@summaconsultores.co.cr">info@summaconsultores.co.cr</a></li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5"></div>
			<div class="col-md-7">
				<p> &#169; Summa Consultores <?php echo date('Y'); ?>.  Todos los derechos reservados.  </p>
			</div>
		</div>
	</div>
	<span class="footer-decor"></span>
</footer>
<!-- footer end    -->
</div>
<!-- content  end  -->
</div>
<!-- content-holder  end  -->
</div>
<!-- wrapper end -->
			</div>
			<!-- Main end -->
			<!--=============== google map ===============-->
			<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
			<!--=============== scripts  ===============-->
			<script type="text/javascript" src="js/jquery.min.js"></script>
			<script type="text/javascript" src="js/plugins.js"></script>
			<script type="text/javascript" src="js/core.js"></script>
			<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>