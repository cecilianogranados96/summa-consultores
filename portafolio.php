<?php include 'header.php'; ?>		
<!--=============== wrapper ===============-->
<div id="wrapper">
	<div class="content-holder elem scale-bg2 transition3" >
		<div class="content  mm">
			<div class="filter-holder fixed-filter">
				<div class="gallery-filters vis-filter">
					<a href="#" class="gallery-filter gallery-filter-active"  data-filter="*">Todos</a>
				
					<?php 	$directorio = opendir("proyectos/"); 
						while ($archivo = readdir($directorio)) 
						{
							if (!is_dir($archivo) && $archivo != "." && $archivo != "..") 
							{
								$nombre = explode(" ",$archivo);
								echo '<a href="#" class="gallery-filter " data-filter=".'.$nombre[1].'">'.$nombre[1].'</a>';
							}
						}
						
					?>
					<!--
						<a href="#" class="gallery-filter " data-filter=".houses">Bancaria</a>
						<a href="#" class="gallery-filter" data-filter=".industrial">Hospitalaria</a>
						<a href="#" class="gallery-filter" data-filter=".interior">Industrial</a>
						<a href="#" class="gallery-filter" data-filter=".interior">Turística</a>
						<a href="#" class="gallery-filter" data-filter=".interior">Educativo</a>
					-->
					
				</div>
			</div>
			<div class="wrapper-inner no-padding full-width-wrap">
				<section class="no-padding no-border">
					<div class="gallery-items   hid-port-info">
						<!-- 1 -->
						
						<?php
							
							function obtener_estructura_directorios($ruta){
								if (is_dir($ruta)){
									$gestor = opendir($ruta);
									while (($archivo = readdir($gestor)) !== false)  {
										$ruta_completa = $ruta . "/" . $archivo;
										$ruta_completa2 = $ruta;
										if ($archivo != "." && $archivo != "..") {
											if (is_dir($ruta_completa)) {
												
												obtener_estructura_directorios($ruta_completa);
												} else {
												$resultado = str_replace("proyectos//", "", $ruta_completa2);
												$resultado1 = str_replace("MONSENOR", "MONSEÑOR", $resultado);
												$resultado2 = str_replace("MANANA", "MAÑANA", $resultado1);
												$etiqueta = explode('/',$ruta_completa2);
												$nombre = explode(" ",$etiqueta[2]);
												echo '  <div class="gallery-item '.$nombre[1].'">
												<div class="grid-item-holder">
												<div class="box-item">
												<a href="portafolio_descripcion.php?path='.$ruta_completa.'" class="ajax portfolio-link">
                                                <span class="overlay"></span>
												<img  src="'.$ruta_completa.'"   alt="">
												</a>
												</div>
												<div class="grid-item ">
                                                <h3><a href="portafolio_descripcion.php?path='.$ruta_completa.'" class="ajax portfolio-link">'.$etiqueta[3].'</a></h3>
                                                <span>'.$nombre[1].'</span>
												</div>
												</div>
												</div>';
												break;
											}
										}
									}
									closedir($gestor);
								} 
							}
							obtener_estructura_directorios("proyectos/");
						?>
						
						
						
						
					</div>
					<!-- end gallery items -->
				</section>
			</div>
		</div>
	</div>
</div>
<!-- wrapper end -->
</div>

<!--=============== google map ===============-->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<!--=============== scripts  ===============-->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/plugins.js"></script>
<script type="text/javascript" src="js/core.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
</body>

</html>