	<?php include 'header.php'; ?>		
						
            <!--=============== wrapper ===============-->
            <div id="wrapper">
                <div class="content-holder elem scale-bg2 transition3" >
                    <div class="content full-height">
                        <div class="swiper-container" id="horizontal-slider" data-mwc="1" data-mwa="0">
                            <div class="swiper-wrapper">
                                <!--=============== 1 ===============-->
                                <div class="swiper-slide">
                                    <div class="bg" style="background-image:url(fotos/KPMG%20Costa%20Rica/1.JPG)"></div>
                                    <div class="overlay"></div>
                                    <div class="slide-title-holder">
                                        <div class="slide-title">
                                            <h3 class="transition">  <a class="ajax transition2" href="portafolio.php">Diseño que enamora</a>  </h3>
                                            <h4>Cada espacio lleva lo mejor de nosotros</h4>
                                        </div>
                                    </div>
                                </div>
                                <!--=============== 2 ===============-->
                                <div class="swiper-slide">
                                    <div class="bg" style="background-image:url(fotos/Junta%20de%20Pensiones%20del%20Magisterio%20Nacional/6.JPG)"></div>
									<div class="overlay"></div>
                                    <div class="slide-title-holder">
                                        <div class="slide-title">
                                            <h3 class="transition">  <a class="ajax transition2" href="portafolio.php">Pasión en cada Detalle</a>  </h3>
                                            <h4>Cada espacio lleva lo mejor de nosotros</h4>
                                        </div>
                                    </div>
                                </div>
                                <!--=============== 3 ===============-->
                                <div class="swiper-slide">
                                    <div class="bg" style="background-image:url(fotos/Vida%20Plena/7.jpg)"></div>
                                    <div class="overlay"></div>
                                    <div class="slide-title-holder">
                                        <div class="slide-title">
                                            <h3 class="transition">  <a class="ajax transition2" href="portafolio.php">Su inversión en buenas manos</a>  </h3>
                                            <h4>Cada espacio lleva lo mejor de nosotros</h4>
                                        </div>
                                    </div>
                                </div>
                                <!--=============== 4 ===============-->
                                <div class="swiper-slide">
                                    <div class="bg" style="background-image:url(fotos/Vida%20Plena/1.jpg)"></div>
                                    <div class="overlay"></div>
                                    <div class="slide-title-holder">
                                        <div class="slide-title">
                                            <h3 class="transition">  <a class="ajax transition2" href="portafolio.php">Experiencia en cada espacio</a>  </h3>
                                            <h4>Cada espacio lleva lo mejor de nosotros</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-nav-holder hor">
                                <a class="swiper-nav arrow-left transition " href="#"><i class="fa fa-long-arrow-left"></i></a>
                                <a class="swiper-nav  arrow-right transition" href="#"><i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="pagination"></div>
                    </div>
                </div>
            </div>
            <!-- wrapper end -->
			
	
			
        </div>
        <!-- Main end -->
        <!--=============== google map ===============-->
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/core.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
    </body>

</html>