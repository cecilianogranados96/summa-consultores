<html>
	<head>
        <meta charset="UTF-8">
        <title>Summa Consultores</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="autor" content="Jose Andres Ceciliano Granados para Sense Digital Agency 2016"/>
        <link type="text/css" rel="stylesheet" href="css/reset.css">
        <link type="text/css" rel="stylesheet" href="css/plugins.css">
        <link type="text/css" rel="stylesheet" href="css/style.css">
        <link rel="shortcut icon" href="images/ico.png">
    </head>
    <body>
        <div class="loader"><i class="fa fa-refresh fa-spin"></i></div>
        <div id="main">
            <header>
                <div class="header-inner">
                    <div class="logo-holder">
                        <a href="index.php" class="ajax"><img src="images/logo.jpg" style="width: 200px;height: 45px;margin-left: 30px;"></a>
                    </div>
                    <div class="nav-button-holder">
                        <div class="nav-button vis-m"><span></span><span></span><span></span></div>
                    </div>
                    <div class="nav-holder">
                        <nav>
                            <ul><!--act-link-->
                                <li><a href="index.php" class="ajax" >Inicio</a></li>
                                <li><a href="portafolio.php" class="ajax ">Proyectos</a></li>
								<li><a href="servicios.php" class="ajax">Servicios</a></li>
								<li><a href="nosotros.php" class="ajax ">Nosotros</a></li>
                                <li><a href="contacto.php" class="ajax">Contacto</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="share-container isShare"></div>
                <a class="selectMe shareSelector transition">Compartir</a>
            </header>