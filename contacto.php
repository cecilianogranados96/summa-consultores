<?php include 'header.php'; ?>		


<!-- header end-->
<!--=============== wrapper ===============-->
<div id="wrapper">
	<div class="content-holder elem scale-bg2 transition3" >
		<div class="content">
			<!-- background animation  -->
			<div class="bg-animate"><img src="images/body-bg.png"  class="respimg" alt=""></div>
			<!-- wrapper-inner -->
			<div class="wrapper-inner">
				<div class="container">
					<div class="page-title no-border">
						<h2>HACEMOS GRANDES SUEÑOS CONVERSEMOS.</h2>
						<h3><span>CONFIANZA - EMPATIA - COMUNICACIÓN FLEXIBLE - AMISTAD - APOYO - RECOMPRA</span></h3>
					</div>
					<!-- map  
					<section class="no-border">
						<div class="map-box">
							<div id="map_addresses" class="map" data-latitude="9.9056481" data-longitude="-84.1365476" data-location="Plaza San Antonio">
							</div>
						</div>
					</section>
					map  end-->
					<!-- contact info  -->
					<section class="no-border">
						<div class="contact-details">
							<div class="row">
								<div class="col-md-4">
									<h3>Detalles de contacto : </h3>
								</div>
								<div class="col-md-3">
									<h4>San José, Costa Rica</h4>
									<ul>
										<li>+ Tel: (506) 2282+3822</li>
										<li>+ Fax: (506) 2282+8476</li>
										
										<li>Apdo. Postal 12187-1000</li>
										<li>San José - Costa Rica</li>
										
									</ul>
								</div>
								<div class="col-md-3">
									<h4>Encuetranos : </h4>
									<ul>
										<li><a target="_blanck" href="https://www.facebook.com/Summa-Consultores-1374283326182095/?fref=ts&__mref=message_bubble">Facebook</a></li>
										<li><a target="_blanck" href="https://www.facebook.com/Summa-Consultores-1374283326182095/?fref=ts&__mref=message_bubble">Twitter </a></li>
										<li><a target="_blanck" href="https://www.facebook.com/Summa-Consultores-1374283326182095/?fref=ts&__mref=message_bubble">Instagram</a></li>
									</ul>
								</div>
								<div class="col-md-12">
									<h4><center><a href="mailto:info@summaconsultores.co.cr">info@summaconsultores.co.cr</a></center></h4>
									
								</div>
							</div>
						</div>
					</section>
					<!-- contact info  end-->
					<!-- contact form -->
					<section>
						<div class="contact-form-holder">
							<div id="contact-form">
								<div id="message"></div>
								<form method="post" action="process.php" name="contactform" id="contactform">
									<input name="name" type="text" id="name"  onClick="this.select()" placeholder="Nombre completo" >
									<input name="email" type="text" id="email" onClick="this.select()" placeholder="E-mail" >
									<input name="asunto" type="text" id="asunto" onClick="this.select()" placeholder="Asunto" >
									<textarea name="comments"  id="comments" onClick="this.select()" >Mensaje</textarea>
									<button type="submit"  id="submit"><span>Enviar </span> <i class="fa fa-long-arrow-right"></i></button>
								</form>
							</div>
						</div>
					</section>
					<!-- contact form  end-->
				</div>
			</div>
			<!-- wrapper inner end   -->
			<!-- parallax column   -->
			<div class="img-wrap">
				<div class="bg" style="background-image: url(fotos/Junta%20de%20Pensiones%20del%20Magisterio%20Nacional/5.JPG)"  data-top-bottom="transform: translateY(300px);" data-bottom-top="transform: translateY(-300px);"></div>
			</div>
			<!-- parallax column end   -->
			<!--to top    -->
			<div class="to-top">
				<i class="fa fa-long-arrow-up"></i>
			</div>
			<!-- to top  end -->
			
			
			
			
			<?php include 'fotter.php'; ?>		
			
			
			
			
			
				