	<?php include 'header.php'; ?>
            <!--=============== wrapper ===============-->	
            <div id="wrapper">
                <div class="content-holder elem scale-bg2 transition3" >
                    <div class="content">
                        <!-- background animation  -->		
                        <div class="bg-animate"><img src="images/body-bg.png"  class="respimg" alt=""></div>
                        <!-- wrapper inner -->	
                        <div class="wrapper-inner">
                            <section class="no-padding no-border" id="sec1">
                                <!-- page title -->		
                                <div class="container">
                                    <div class="page-title no-border">
                                        <h2>CALIDAD EN CADA DETALLE, Y EN CADA ESPACIO</h2>
                                        <h3><span>Siempre ponemos valor en cada detalle, en cada espacio que trabajamos.</span></h3>
                                    </div>
                                </div>
                            </section>
                           	<div class="container">
					<section class="no-border">
						<div class="full-width-holder">
							<div class="fullwidth-slider-holder">
								<div class="customNavigation">
									<a class="next-slide transition"><i class="fa fa-long-arrow-right"></i></a>
									<a class="prev-slide transition"><i class="fa fa-long-arrow-left"></i></a>
								</div>
								<div class="full-width owl-carousel">
									<!-- 1 -->
									<div class="item">
										<img src="fotos/Pribal%20Bank/1.jpg" class="respimg" alt="">
									</div>
									<!-- 2 -->
									<div class="item">
										<img src="http://localhost/Summa/fotos/Vida%20Plena/1.jpg" class="respimg" alt="">
									</div>
									<!-- 3 -->
									<div class="item">
										<img src="fotos/Junta%20de%20Pensiones%20del%20Magisterio%20Nacional/6.JPG" class="respimg" alt="">
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- full width slider end -->
					<section>
						<div class="row">
							<div class="col-md-12">
								
								
								
								<p class="texto_azul" ><b>SUMMA CONSULTORES</b></p>
								<br><br>
								<p style="align: justify;">
Empresa experta en el área de diseño arquitectónico con más 15 años de experiencia en la gestión integral de proyectos de diseño y construcción, traduciendo a diseño las necesidades de nuestros clientes en soluciones novedosas en armonía con el ambiente.<BR>
Somos una empresa que tiene gran experiencia en el área hospitalaria, bancaria, industrial,  comercial,   de  oficinas,  hotelero  y  turístico.  Nos consolidamos gracias a la sinergia de la experiencia y el talento humano de cada uno de sus miembros de equipo de trabajo y el aporte estratégico de consultores externos en diferentes ramas, lo cual hace que nuestro modelo de negocio sea muy flexible.<BR>
Uno de nuestros mayores retos es comprender el proceso de negocio de nuestros clientes para así comprender a sus clientes y de esta forma lograr la satisfacción de sus expectativas más allá de los estándares del mercado nacional, cuidando con rigor cada detalle de trabajo, estableciendo precios justos y un trato personalizado.<BR>
Summa Consultores  ha  ganado  en  dos  ocasiones  el  premio  René Frank y  el  premio  a  la  excelencia   por  el  International  Quality  Summit  de  New York. <BR>
Una  de  las mayores virtudes de su Gerente General, el Arq. Esteban Sandí, es el excelente  liderazgo que ejecuta   en el trabajo interdisciplinario, factor clave para llevar con éxito los proyectos.<BR>

								
						<BR><BR><BR>				
	

<BR>
<b>Nuestra misión:</b><BR><BR>
Hacer propia la operación de nuestro cliente, para poder entender no solo sus necesidades sino también las necesidades de sus clientes y de forma transformar o crear espacios para vivir, reunirse, estudiar, disfrutar, trabajar y producir. <BR>
Promovemos el desarrollo del trabajo en equipo, una actitud emprendedora y un espíritu innovador que  proporcione soluciones  de alto valor agregado  tanto a instituciones gubernamentales como al sector privado.<BR>
<BR><BR>

<b>Nuestra Visión:</b><BR><BR>
Ser la mejor opción en Costa Rica como alineado de negocio, ayudando a nuestro cliente con diseños integrales en arquitectura, ingeniería, administración de proyectos y ramas afines, entregando productos de alta calidad y creando así relaciones a largo plazo.<BR><BR>
<BR><BR>
</p>
<div class="texto_azul" style='font-size: 50px;'>Los valores de la gente que nos gusta:</div><BR><BR>
<p style="align: justify;">
- Nos gusta la gente que Sorprenda, que vayan más allá de la expectativa de si mismos y de nuestros clientes.
<BR><BR>
- Nos gusta la gente que Una, que pueda trabajar con diferentes ramas como un único ente completo e integral, éxito de nuestro trabajo en equipo.
<BR><BR>
- Nos gusta la gente que está en Movimiento de forma constante, lo que le permite adaptarse a los cambios.
<BR><BR>
- Nos gusta la gente de Mente abierta, que considere todas las posiciones y puntos de vista, que analice todas las perspectivas, evalúe alternativas y descubra nuevos caminos y soluciones.
<BR><BR>
- Nos gusta la gente con Actitud que le de la fuerza permite llegar a la mejorar y a dar lo mejor de sí mismo.
<BR><BR>
</p>
		<BR><BR>						
								
								
								
								
							</div>
						</div>
					</section>
					
				</div>
			</div>
			<!-- wrapper inner end   -->
			<!-- parallax column   -->
			<div class="img-wrap">
				<div class="bg" style="background-image: url(fotos/Vida%20Plena/3.JPG)"  data-top-bottom="transform: translateY(300px);" data-bottom-top="transform: translateY(-300px);"></div>
			</div>
			<!-- parallax column end   -->
			<!--to top    -->
			<div class="to-top">
				<i class="fa fa-long-arrow-up"></i>
			</div>
			<!-- to top  end -->
			<?php include 'fotter.php'; ?>		
