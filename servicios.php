<?php include 'header.php'; ?>		

<!-- header end-->	
<!--=============== wrapper ===============-->	
<div id="wrapper">
	<div class="content-holder elem scale-bg2 transition3" >
		<div class="content">
			<!-- background animation  -->		
			<div class="bg-animate"><img src="images/body-bg.png"  class="respimg" alt=""></div>
			<!-- wrapper inner -->	
			<div class="wrapper-inner">
				<section class="no-padding no-border" id="sec1">
					<!-- page title -->		
					<div class="container">
						<div class="page-title no-border">
							<h2>NOS APASIONA CREAR EXPERIENCIAS UNICAS.</h2>
							<h3><span>Siempre ponemos valor en cada detalle, en cada espacio que trabajamos.</span></h3>
						</div>
					</div>
				</section>
				<div class="container">
					<section class="no-border">
						<div class="full-width-holder">
							<div class="fullwidth-slider-holder">
								<div class="customNavigation">
									<a class="next-slide transition"><i class="fa fa-long-arrow-right"></i></a>
									<a class="prev-slide transition"><i class="fa fa-long-arrow-left"></i></a>
								</div>
								<div class="full-width owl-carousel">
										<!-- 2 -->
									<div class="item">
										<img src="fotos/Vida Plena/7.JPG" class="respimg" alt="">
									</div>
									<!-- 1 -->
									<div class="item">
										<img src="fotos/Junta de Pensiones del Magisterio Nacional/9.jpg" class="respimg" alt="">
									</div>
								
									
								</div>
							</div>
						</div>
					</section>
					<!-- full width slider end -->
					<section>
						<div class="row">
							<div class="col-md-12">
								
								
								
								<p class="texto_azul" ><b>NUESTROS SERVICIOS:</b></p>
								<br><br>
								<p>
									
									Creamos atmósferas para su estilo de vida.
									Contamos con un equipo de gran talento y experiencia, al cual también complementamos con el aporte estratégico de consultores externos que no solo nos asegura una participación exitosa en el proceso de de la administración de diferentes proyectos sino también nos brinda una estructura flexible y ágil a las diferentes necesidades de nuestros clientes y a sus presupuestos, ya que este modelo de negocio nos permite precios razonables de alta calidad.
									Dentro de nuestro portafolio de servicios de consultoría a nivel nacional e internacional ofrecemos a nuestros clientes:
								</p>
								<p>
									<br>
									<b>Gestión integral de proyectos</b><br><br>
									-Estudios de factibilidad técnica<br>
									-Planeamiento y diseño de proyectos<br>
									-Estimación de costos de obra<br>
									-Especificaciones y documentos de licitación y contratación<br>
									-Inspección técnica y administración de proyectos de construcción<br>
									-Consultoría general y asistencia técnica<br>
									-Due diligence<br><br>
									<b>Servicios en arquitectura</b><br><br>
									-Estudios Preliminares<br>
									-Anteproyecto<br>
									-Diseño arquitectónico<br>
									-Especificaciones Técnicas<br>
									-Inspección de obras<br>
									-Dirección Técnica<br>
									-Administración de proyectos<br>
									-Estimación de presupuestos: costos y análisis<br><br>
									<b>Arquitectura de paisaje</b><br><br>
									-Planificación y diseño de proyectos<br>
									-Conservación y rehabilitación de espacio público, espacios abiertos y el suelo<br>
									-Restauración medioambiental<br>
									-Planificación de parques y de espacios de recreación<br>
									-Conservación histórica.<br>
									-Diseño  de interiores<br>
									-Planificación, gestión y dirección de proyectos de diseño del espacio interno.<br>
									-Diseño de espacios para la vida doméstica, pública y laboral con el equipamiento que cada uno necesita para ser habitado a plenitud, para mejorar la vida, incrementar la productividad y proteger la salud de los usuarios.<br>
									-Asesoría de las últimas tendencias y avances de la construcción, materiales, mobiliario y complementos.<br><br>
									<b>Diseño estructural</b><br><br>
									-Análisis, diseño estructural y sismorresistente<br>
									-Arbitraje y peritaje<br>
									-Informes técnicos sobre ingeniería forense<br>
									-Evaluación de daños causados por sismo e incendio<br>
									-Estudios de amenaza y vulnerabilidad sísmica<br>
									-Adecuación y refuerzo sísmico<br>
									-Estudio de riesgo y seguro contra terremoto<br><br>
									<b>Diseño Eléctrico</b><br><br>
									-Distribución en Baja Tensión<br>
									-Distribución en Media Tensión<br>
									-Respaldo continuo<br>
									-Iluminación<br>
									-Análisis de rendimiento lumínico<br>
									-Análisis de corto circuito<br>
									-Coordinación de disparos<br>
									-Simulación tridimensional<br>
									-Infraestructura telecomunicaciones<br>
									-Infraestructura media tensión<br>
									-Voz y datos<br>
									-CCTV<br>
									-LSS<br><br>
									<b>Diseño mecánico</b><br><br>
									-Aire acondicionado y ventilación<br>
									-Protección contra incendio<br>
									-Facilidades industriales (gases, agua, procesos)<br>
									-Administración y Conservación de Energía<br>
									-Análisis de ciclo de vida económica<br>
									-Simulación de flujos compresibles<br>
									-Simulación tridimensional<br>
									-Análisis de ruido<br>
									-Infraestructura Pluvial<br>
									-Infraestructura Sanitaria<br>
									-Infraestructura Potable<br>
									-Infraestructura protección incendio<br>
									-Plomería<br><br>
									<b>Avalúos</b><br><br>
									-Avalúos Comerciales<br>
									-Avalúos Bancarios<br>
									-Avalúos Fiscales<br>
									-Avalúos Catastrales<br><br>
									<b>Tramitología de permisos</b><br><br>
									-Permisos municipales<br>
									-Ministerio de Salud<br>
									-Colegio Federado de Ingenieros y Arquitectos<br>
									-INVU<br>
									-Servicios públicos: agua, luz y telecomunicaciones<br>
									-SETENA<br>
									-Ingeniería de Bomberos<br>
									-Desfogue Pluvial<br>
									-Otros<br><br>
									<b>Inspección de obra</b><br><br>
									-Control y ejecución de la obra según planos y tiempo estimado<br>
									-Estricto control de materiales y acabados<br>
									-Control de costos<br>
									-Verificación de permisos<br>
									-Informes de avance de obras<br><br>
									
								</p>
								
								
								
							</div>
						</div>
					</section>
					
				</div>
			</div>
			<!-- wrapper inner end   -->
			<!-- parallax column   -->
			<div class="img-wrap">
				<div class="bg" style="background-image: url(fotos/Pribal%20Bank/2.jpg)"  data-top-bottom="transform: translateY(300px);" data-bottom-top="transform: translateY(-300px);"></div>
			</div>
			<!-- parallax column end   -->
			<!--to top    -->
			<div class="to-top">
				<i class="fa fa-long-arrow-up"></i>
			</div>
			<!-- to top  end -->
			<?php include 'fotter.php'; ?>		
